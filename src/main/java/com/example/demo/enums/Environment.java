package com.example.demo.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Environment {

    DEV, STAGE, PROD;
}
