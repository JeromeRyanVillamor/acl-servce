package com.example.demo.reository;

import com.example.demo.enums.ApplicationList;
import com.example.demo.enums.Environment;
import com.example.demo.model.Acl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AclRepository extends JpaSpecificationExecutor<Acl>, JpaRepository<Acl, Long> {

    Acl queryByNameAndIpAndEnvironment(ApplicationList name, String ip, Environment environment);
    boolean existsByNameAndIpAndEnvironment(ApplicationList name, String ip, Environment environment);
}
