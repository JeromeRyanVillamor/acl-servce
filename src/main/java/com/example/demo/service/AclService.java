package com.example.demo.service;

import com.example.demo.dto.AclRequest;
import com.example.demo.exception.AclEntityNotFoundException;
import com.example.demo.model.Acl;
import com.example.demo.reository.AclRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AclService {

    @Autowired
    private AclRepository aclRepository;

    public Acl get(AclRequest aclRequest) {
        Acl acl = aclRepository.queryByNameAndIpAndEnvironment(
                aclRequest.getName(),
                aclRequest.getIp(),
                aclRequest.getEnvironment()
        );

        if(acl == null) {
            throw new AclEntityNotFoundException();
        }

        return acl;
    }
}
