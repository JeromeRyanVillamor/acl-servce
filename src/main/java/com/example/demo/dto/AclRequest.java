package com.example.demo.dto;

import com.example.demo.enums.ApplicationList;
import com.example.demo.enums.Environment;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
public class AclRequest {

    @NotNull(message = "name is required")
    private ApplicationList name;

    @NotEmpty(message = "ip is required")
    @Pattern(regexp = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", message = "IP is invalid")
    private String ip;

    @NotNull(message = "environment is required")
    private Environment environment;
}
