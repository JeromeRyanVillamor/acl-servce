package com.example.demo.dto;

import com.example.demo.enums.ApplicationList;
import com.example.demo.enums.Environment;
import lombok.Data;

@Data
public class AclResponse {

    private Credentials credentials = new Credentials();
    private boolean status;
    private String message;

    public static AclResponse success(AclRequest aclRequest, String message) {
        AclResponse response = new AclResponse();
        response.credentials.name = aclRequest.getName();
        response.credentials.ip = aclRequest.getIp();
        response.credentials.environment = aclRequest.getEnvironment();
        response.status = true;
        response.message = message;

        return response;
    }

    public static AclResponse error(AclRequest aclRequest, String message) {
        AclResponse response = new AclResponse();
        response.credentials.name = aclRequest.getName();
        response.credentials.ip = aclRequest.getIp();
        response.credentials.environment = aclRequest.getEnvironment();
        response.status = false;
        response.message = message;

        return response;
    }

    @Data
    public static class Credentials {
        private ApplicationList name;
        private String ip;
        private Environment environment;
    }
}
