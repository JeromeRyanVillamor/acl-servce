package com.example.demo.model;

import com.example.demo.enums.ApplicationList;
import com.example.demo.enums.Environment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Acl {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private ApplicationList name;
    private String ip;
    private Environment environment;
    private boolean isAuthorized;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public Acl(ApplicationList name, String ip, Environment environment) {
        this.name = name;
        this.ip = ip;
        this.environment = environment;
        this.isAuthorized = true;
    }
}
