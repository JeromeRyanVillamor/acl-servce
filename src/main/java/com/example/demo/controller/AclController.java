package com.example.demo.controller;

import com.example.demo.service.AclService;
import com.example.demo.dto.AclRequest;
import com.example.demo.dto.AclResponse;
import com.example.demo.enums.ApplicationList;
import com.example.demo.enums.Environment;
import com.example.demo.exception.AclEntityNotFoundException;
import com.example.demo.model.Acl;
import com.example.demo.reository.AclRepository;
import net.kaczmarzyk.spring.data.jpa.domain.EqualIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AclController extends BaseController {

    @Autowired
    private AclRepository aclRepository;

    @Autowired
    private AclService aclService;

    @GetMapping(value = "/v1/acl")
    public List<Acl> index(@And({
            @Spec(path = "name", spec = EqualIgnoreCase.class),
            @Spec(path = "ip", spec = EqualIgnoreCase.class),
            @Spec(path = "environment", spec = EqualIgnoreCase.class)
    }) Specification<Acl> customerSpec) {
        return aclRepository.findAll(customerSpec);
    }

    @PostMapping(value = "/v1/acl")
    public ResponseEntity<AclResponse> store(@Valid @RequestBody AclRequest aclRequest) {
        ApplicationList name = aclRequest.getName();
        String ip = aclRequest.getIp();
        Environment environment = aclRequest.getEnvironment();

        boolean isExists = aclRepository.existsByNameAndIpAndEnvironment(name, ip, environment);
        if(isExists) {
            return this.response(aclRequest, HttpStatus.UNPROCESSABLE_ENTITY, "Combination of app-name, ip and environment already existed in database.");
        }

        aclRepository.save(new Acl(name, ip, environment));
        return this.response(aclRequest, HttpStatus.OK, "Successfully Created.");
    }

    @PatchMapping(value = "/v1/acl/enable")
    public ResponseEntity<AclResponse> enable(@Valid @RequestBody AclRequest aclRequest) {
        try {
            Acl acl = aclService.get(aclRequest);
            acl.setAuthorized(true);
            aclRepository.save(acl);
            return this.response(aclRequest, HttpStatus.OK, "Successfully Enabled.");
        } catch (AclEntityNotFoundException e) {
            return this.response(aclRequest, HttpStatus.UNPROCESSABLE_ENTITY, "Entity not found.");
        }
    }

    @PatchMapping(value = "/v1/acl/disable")
    public ResponseEntity<AclResponse> disable(@Valid @RequestBody AclRequest aclRequest) {
        try {
            Acl acl = aclService.get(aclRequest);
            acl.setAuthorized(false);
            aclRepository.save(acl);
            return this.response(aclRequest, HttpStatus.OK, "Successfully Disabled.");
        } catch (AclEntityNotFoundException e) {
            return this.response(aclRequest, HttpStatus.UNPROCESSABLE_ENTITY, "Entity not found.");
        }
    }

    @DeleteMapping(value = "/v1/acl")
    public ResponseEntity<AclResponse> destroy(@Valid @RequestBody AclRequest aclRequest) {
        try {
            Acl acl = aclService.get(aclRequest);
            aclRepository.delete(acl);
            return this.response(aclRequest, HttpStatus.OK, "Successfully Deleted.");
        } catch (AclEntityNotFoundException e) {
            return this.response(aclRequest, HttpStatus.UNPROCESSABLE_ENTITY, "Entity not found.");
        }
    }

    @PostMapping(value = "/v1/auth-check")
    public ResponseEntity<AclResponse> authCheck(@Valid @RequestBody AclRequest aclRequest) {
        try {
            Acl acl = aclService.get(aclRequest);

            if(! acl.isAuthorized()) {
                return new ResponseEntity<>(
                        AclResponse.error(aclRequest, "Request is invalid"),
                        HttpStatus.UNAUTHORIZED
                );
            }

            return this.response(aclRequest, HttpStatus.OK, "Successfully Whitelisted");
        } catch (AclEntityNotFoundException e) {
            return this.response(aclRequest, HttpStatus.UNPROCESSABLE_ENTITY, "Entity not found.");
        }
    }
}
