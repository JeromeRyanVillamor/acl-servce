package com.example.demo.controller;

import com.example.demo.dto.AclRequest;
import com.example.demo.dto.AclResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseController {

    protected ResponseEntity<AclResponse> response(AclRequest aclRequest, HttpStatus status, String message) {
        return new ResponseEntity<>(
                AclResponse.success(aclRequest, message),
                status
        );
    }
}
