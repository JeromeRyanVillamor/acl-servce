# ACL Service

Access Control Layer service. Used to check if given IP address has access to a specific app and environment.

## Requirements
- Java 8 or higher
- Maven

## API
- Whitelist IP address.
- Enable and disable IP address.
- Get all IP addresses.
- Delete IP address.
- Perform AUTH check.

You may try to play these API's using the postman collection I attached in the repository.

## Usage
Just as your normal spring-boot, you can run the application using your favorite IDE or using spring-boot:start on your terminal:

```java
mvn spring-boot:start
```

## Question?
- Does Unit/Integration tests required? If yes, kindly ping me  at jeromeryan.villamor@gmail.com.